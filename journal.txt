<2019-08-10 sob 21:56>

Chcę ogarnąć swoją stronę internetową http://gumen.pl.  Obecnie jest
mało interesująca, mało przydatna i zabałaganiona.  Część plików
znajduje się na serwerze FTP, część na SSH.  Jedna wersja w protokole
HTTP, druga GOPHER.  Część rzeczy nie działa i niektóre pliki są
niedostępne mimo iż powinny być (np filmy i tutoriale).

<2019-08-10 sob 23:28>

Zanim cokolwiek zrobię muszę się zastanowić i ustalić kilka rzeczy.

- WHY  --> Jaki jest cel tej strony?
- HOW  --> Jak zamierzam go osiągnąć?
- WHAT --> Czego użyję?

Jak zwykle, odpowiedź na WHAT jest najprostsza i najmniej istotna.

<2019-08-11 nie 00:09>

Po namyśle.  Głównym celem strony jest promowanie mojej osoby.  Zawsze
kiedy o tym myślę to rozważam temat prywatności w sieci.  Powoduje to,
że chcę wrzucać swoje prace i pisać artykuły, ale pod pseudonimem.
Dlatego jest gumen.pl a nie <imię i nazwisko>.pl.

<2019-08-11 nie 07:11>

Problem z tym podejściem jest taki, że i tak mam CV i link do Linkedin
gdzie widnieje moje pełne imię i nazwisko.  Więc równie dobrze te dane
mógłbym spokojnie używać wszędzie.  Mieć oficjalne konto na github'ie,
używać oficjalnego maila z gmail'a itd.  Moja obecność w sieci nie
jest w pełni anonimowa.

Mogę teraz pójść w dwóch kierunkach.  Albo podjąć starania żeby
udostępniać tylko rzeczy pod pseudonimem, albo olać to i się nie
przejmować.  Ciężko wybrać.  Najłatwiej byłoby udostępniać wszystko co
popadnie zapominając o zagrożeniach.

Przykładowo gdybym faktycznie miał stronę.  Tytułował się na niej
imieniem i nazwiskiem.  Może dojść do sytuacji, że jakiś artykuł,
jakaś praca, jakiś filmik, cokolwiek się komuś nie spodoba bo np
obraża jego uczucia (popularny powód w dzisiejszych czasach) to może
próbować mi zaszkodzić.  Ludzie robię przeróżne rzeczy od modyfikacji
zdjęć, robienia sztucznych kont internetowych po wzywanie policji pod
twój adres.

Dlatego, żebym w ogóle nie musiał się nigdy martwić takimi rzeczami,
lepiej iść w pseudonim.  Imię i nazwisko jest mi potrzebny jeśli
staram się o pracę, albo szukam rozgłosu.  W przypadku pierwszego mogę
sam rozsyłać CV czy adres do mojej strony, albo firma może się ze mną
kontaktować nie wiedząc jak mam na imię.  W przypadku drugiego to też
nie problem bo rozgłos mnie nie interesuje, a wręcz przeciwnie.

<2019-08-11 nie 07:45>

Przeglądam obecną stronę i szukam powiązań z "światem zewnętrznym".
Szczęściem nie ma tego wiele i już większość usunąłem.  Niestety, w
niektórych repozytoriach na gitlab'ie widnieją commity od nazwy
użytkownika zawierającej moje pełne imię i nazwisko.  Trzeba się
trochę naszukać żeby to znaleźć, ale da się.  Myślę, że na chwilę
obecną ma to malutki priorytet, ale warto stworzyć listę zadań.  Dodać
pierwsze zadanko żeby nie zapomnieć.

<2019-08-11 nie 08:21>

Zrobiłem listę zadań i od razu pozbyłem się problemu.

<2019-08-11 nie 12:54>

Dobra czyli to mam ustalone.  Strona ma promować moją osobę, ale pod
pseudonimem.

Promowanie polegać będzie na tradycyjnym portfolio, listami nagranych
filmów i tutoriali, linkami do rzeczy, które są dla mnie interesujące
i przydatne.  Chciałbym udostępniać pewne treści regularnie.  Miałem
pomysł żeby na bieżąco aktualizować tabelę z ilością powtórzeń różnych
ćwiczeń wykonanych każdego dnia.  Dla siebie żeby śledzić i liczyć
postęp oraz jako ciekawostka dla innych, którzy wiedzą do kogo należy
ta strona.  Dzięki temu będzie powód żeby częściej na nią zaglądać.
Prowadząc stronę pod pseudonimem nie będę się wahał udostępniać takie
treści, podobnie jak np artykuły na różne tematy, które chciałbym
pisać, ale często obawiam się, że odbiorca będzie je zbyt utożsamiał z
moją osobą, co nie jest moją intencją.

Następną sprawą jest kwestia techniczna strony.  Niewygodnie piszę się
w samym HTML'u.  Trudno się też zmienia strukturę strony jeśli pojawia
się chęć wprowadzenia zmian.  Dużo lepiej jest mieć CMS.  Trzymać
treść pod postacią samych danych i renderować je do HTML'a.  Pod
żadnym pozorem nie chcę używać WordPress'a i innych podobnych
wynalazków.  Planuję spróbować wykorzystać "Publishing" z org-mode.
Przechowywanie treści strony pod postacią zwykłych plików tekstowych
jest dla mnie bardzo atrakcyjna.  Zresztą nie tylko dla mnie.  GitHub
i GitLab mają tak zwane "Pages" co jest dokładnie tym samym, ale dla
plików Markdown znajdujących się w wybranym repozytorium.

Chcę zrobić podobną rzecz.  Proste publiczne repozytorium z plikami
org, katalogiem z szablonami, obrazkami, filmami i innymi plikami.

Jeszcze nigdy nie używałem opcji "Publish".  Muszę przeczytać
dokumentację i zrobić prosty test.

<2019-09-06 pią 13:06>

Przetestowałem "Org Publishing".  Bardzo fajne, ale nie jest to
rozwiązanie dla mnie.  Dlatego z żalem w sercu porzucam ten pomysł.

----------------------------------------------------------------------

Dużo rozmyślałem nad kwestią techniczną.  Zdecydowałem się, na
napisanie własnego serwera w Node.js.  Chcę mieć stronę z trasowaniem
(ang. routing) na back-end'zie i na front-end'zie jednocześnie.  Tak
żeby używanie strony nie wymagało JS.

Samo repozytorium strony ma być publiczne jako jedna z rzeczy do
portfolio.  Dlatego musi być wzorowym projektem :D Chcę zastosować
TDD.  Nie wiem czy jestem na to gotowy.

<2019-09-16 pon 05:32>

Mam repo i prosty serwer.  Wszystko w TDD.  Na razie ten serwer nic
nie robi.  Muszę się zastanowić co dalej.

Pierwszą rzecz jaką serwer powinien być w stanie zrobić to wyświetlić
statyczne pliki znajdujące się na serwerze.  Od tego zacznę.  Tylko
zastanawiam się nad samym mechanizmem.  Chciałbym docelowo żeby dało
się na mojej stronie wyświetlać też pliki podając pełny adres URL do
innej strony.  Dlatego myślę nad formatem URL.

Chyba najlepiej będzie serwować statyczne pliki normalnie jak zawsze
poprzez zwykłą ścieżkę.  Ale rzeczy wyświetlane na stronie poprzez
query string.

Tak zrobię.

<2019-09-30 pon 06:52>

Trochę to trwało ale w końcu mam serwer serwujący statyczne pliki
napisany według zasad programowania funkcyjnego.  Musiałem opanować
dużo nowych koncepcji czytając książkę, artykuły, dokumentacje oraz
obejrzeć godziny filmów.

Trochę ucierpiały na tym testy.  Nic nie szkodzi.  To się szybko
nadrobi.  Najważniejsze, że moje zrozumienie programowania funkcyjnego
osiągnęło poziom mniejszej ilości "WTF na minutę"
(https://www.osnews.com/story/19266/wtfsm/).

Teraz w planie mam drobne sprzątanie, dopisanie testów i obsłużenie
plików INDEX.

<2019-10-03 czw 06:42>

Dość szybko rozszerzyłem kod o obsługę plików index.html, ale drugi
raz natrafiłem na problem utraty wartości zmiennej w kompozycji
funkcji.  Ostatnim razem nie był aż tak rażący więc go zignorowałem.
Tym razem bardziej mi przeszkadza i obawiam się, że pojawi się jeszcze
wiele razy.  Moja wiedza jest niewystarczająca żeby samemu sprostać
temu zadaniu.  Muszę wrócić do książek i artykułów.

<2019-10-04 pią 08:41>

Nic nie wskórałem studiowaniem dokumentacji biblioteki Ramda.  Dopiero
długa medytacja przyniosła rezultaty.  Pozwoliła dogłębnie zrozumieć
problem i otworzyła mi oczy na jego prawdziwą naturę.  Oświecony nową
wiedzą usiadłem i napisałem czystszy kod.

<2019-10-04 pią 10:06>

Byłem ślepy.  Niewielki błąd wkradł się do kodu.  Naprawienie go
wymagało ponowne zmierzenie się z odwiecznym problemem utraty
oryginalnej wartości zmiennej w kompozycji funkcji.

Tym razem nie było ucieczki.  Czułem, że tej walki nie wygram.  Wtedy
dotarło do mnie, że nie muszę walczyć.  Kiedy pozwolę zmiennej iść tam
gdzie chce mogę zmienić jej kierunek jeśli tego zapragnę.

<2019-10-06 nie 21:13>

Dopisuję brakujące testy i chciałem użyć narzędzia pokazującego
pokrycie plików testami.  Niestety nic nie działa.  Wszystko przez to,
że używam es-modules.  Długo próbowałem to odpalić, ale bez skutku.
Szkoda mi na to czasu.  W ogóle nie chce mi się pisać tych testów.
No ale teraz już nie zrezygnuję.  Napiszę tyle sile się da napisać bez
większych komplikacji i wracam do tworzenia strony.

Niestety następne 7 dni muszę poświęcić moją uwagę pilniejszym
sprawom.  Nie będzie żadnego commita dopóki nie skończę pracować nad
modelami do [[https://dziejekhorinis.org/][Dziejów Khorinis]].

<2019-12-25 śro 21:04>

Tak, ciągle tu jestem.  Długo pracowałem nad modelami do Dziejów
Khorinis.  Skończyłem wszystko co miałem do zrobienia.  Ciągle
powracam do modelowania jak dostaję nowe rzeczy, ale jest już tego
bardzo mało i częściej czekam na odpowiedź od innego grafika niż
faktycznie modeluję.

Po za tym wskoczył mi w grafik projekt "leon", którego zrobienie było
dla mnie ważniejsze od pracy nad modelami.  Dopiero jak skończyłem nad
nim pracować to wróciłem do modeli.

Przez cały czas myślałem nad podejściem do kodowania tego projektu.
Doszedłem do wniosku, że już przetestowałem programowanie funkcyjne.
Nie do końca mi się podoba tak samo jak mam mieszane uczucia co do
testowania wszystkiego tak dogłębnie.  Dlatego zdecydowałem, że wywalę
to wszystko i robię najprościej, najłatwiej jak się da.  Skupię się na
szybkim osiągnięciu zamierzonego efektu zamiast na użytych metodach.

<2019-12-26 czw 08:58>

Pierwsze co zrobię to wyczyszczę projekt z niepotrzebnych rzeczy.
Zostawię tylko te pliki i ten kod, który faktycznie jest potrzebny
żeby dana funkcjonalność działała.

<2019-12-26 czw 09:49>

Zrobione.  Usunąłem niemalże wszystko.  Został tylko jeden skrypt
serwera i jeden plik JSON z mapą typów treści.  Pozbyłem się też
testów automatycznych.  Zdecydowałem, że dodam je dopiero wtedy kiedy
będą mi potrzebne.  Czyli w sytuacji, w której będę często sprawdzać
pewną funkcjonalność albo monitorować czy czegoś nie popsułem.

Ciężko było mi usunąć kod napisany czysto funkcyjnie.  Był taki
piękny.  Doceniłem jego elegancję czytając go po tak długiej przerwie.
Mimo to nadal obstawiam za prostotą.  Teraz mam znacznie mniej kodu.
Można poznać i zrozumieć całą logikę serwera patrząc na jeden plik,
który ma zaledwie 70 linijek kodu.  Tak będzie lepiej.

<2019-12-27 pią 14:01>

Zastanawiam się co teraz powinienem zrobić.  W sensie co ma największy
priorytet.  Teoretycznie serwer jest już w stanie serwować statyczne
pliki i mógłbym napisać stronę główną.  Hmm...  Nawet nie wiem co by
miało na niej być.  To będzie "okno" na inne pliki.

Muszę to przemyśleć i rozrysować na kartkach.

<2019-12-27 pią 14:22>

Dobra już wiem co zrobić.  Zastanawiałem się nad problemem trzymania
treści strony na publicznym repo projektu strony.  Chodzi o to, że sam
projekt strony chcę mieć jako publiczne repozytorium, ale nie chcę
udostępniać treści strony jaką część tego repo.

Plan jest taki: trzymać całą treść strony w odrębnym miejscu serwera
ssh, mieć skrypt budujący, który przemieli wskazany katalog i stworzy
pliki statyczne gotowe do wyświetlania na stronie.  Zbudowane pliki
będą pokryte przez .gitignore.  W ten sposób na repozytorium strony
będzie tylko kod odpowiedzialny za działanie serwera i budowanie
statycznych plików, a same pliki mogę trzymać gdzie chcę.

Teraz zrobię tak, że przygotuję testowy katalog z różnymi plikami na
swoim ThinkPadzie.  Potem zacznę pisać kod odpowiedzialny za
przygotowywanie statycznych plików gotowych do wyświetlenia na
stronie.  Będzie to musiało pokrywać takie przypadki jak wyświetlanie
zawartości katalogu jeśli ten katalog nie posiada pliku index.html.

<2019-12-27 pią 15:04>

Szykowałem się do sporej ilości operacji na plikach.  Robienie tego
standardowo z miliardem callbacków bardzo mnie zniechęcało, ale
zauważyłem w dokumentacji NodeJS, że standardowo wspierają możliwość
robienia tego wszystkiego na promisach.  Dlatego przepisałem
istniejący już kod na async i await.  Musiałem użyć try catch żeby
obsłużyć błędy, ale z dwojga złego lepiej w tę stronę.  Teraz kod jest
jeszcze prostszy i krótszy.

<2020-01-02 czw 05:43>

Nie wyprodukowałem dużo kodu, ale fajnie idzie.  Mógłbym użyć
biblioteki, którą już dobrze znam do manipulacji plikami żeby było
prościej i szybciej, ale zamiast tego wolę napisać własny kod.
Dlatego tyle to trwa.  Mniejsza o to.  Jestem już blisko.

<2020-01-02 czw 13:11>

Tworzenie katalogów z statycznymi plikami serwera na podstawie podanej
ścieżki do plików źródłowych dział.  Bardzo fajnie to funkcjonuje.  Od
biedy można już przeglądać pliki serwera w przeglądarce.

Zastanawiam się co teraz powinno być następnym krokiem.  Myślę, że
dokładnie to samo tylko w formie wyszukiwarki.  Tak, że zawartość
podanej ścieżki wyświetli się na szablonie strony zamiast bezpośrednio
w przeglądarce.

    /path/to/file.txt   ?s=/path/to/file.txt
    +---------------+   +---------------+
    | File content  |   | +-----------+ |
    |               |   | |File con...| |
    |               |   | |           | |
    +---------------+   +---------------+

Dzięki temu będę miał kontrolę nad tym jak ta treść jest prezentowana
co powinno usprawnić UX na telefonach.  Zwłaszcza podczas przeglądania
plików tekstowych; zwykłych jak i Markdown oraz ORG.

Dodatkowo mogę ładnie wyświetlać zawartość katalogów bez konieczności
każdorazowego tworzenia index.html dla danej treści.  Wystarczy, że
stworzę katalog z obrazkami i będę miał galerię, katalog z plikami
tekstowymi i BOOOM mamy bloga.  Przynajmniej taki jest plan.

<2020-01-04 sob 14:34>

Nie podoba mi się sposób w jaki loguję czas pracy.  Logowanie czasu
bezpośrednio do zadanie jakim się zajmuję brzmi sensownie, ale chcę
teraz wypróbować format, którego używałem podczas pracy nad projektem
"Leon".  Był on dużo prostszy przez co nie musiałem się tak bardzo nad
tym zastanawiać.  Zwykłe jedno zadanie zatytułowane "Złota Godzina".
Cały czas pracy loguję do niego.

Teraz np mam dylemat bo wiem dokładnie co chcę zrobić i nie czuję
potrzeby tej całej biurokracji z tworzeniem zadania w quests.org.

<2020-01-04 sob 15:03>

Spędzam więcej czasu na rozmyślaniu niż kodowaniu.

<2020-01-04 sob 15:21>

Nie napisałem za dużo, ale rozumiem już jak chcę żeby to dokładniej
działało.  Cały szkopuł w tym, że chce aby każde zwykłe zapytanie do
serwera zwracało statyczny plik HTML wyświetlający w swojej treści
plik, do którego podałem ścieżkę.

<2020-01-05 nie 08:53>

Po namyśle i wstępnych testach zaobserwowałem, że nie wszystkie
rodzaje plików jakie będę trzymał na serwerze zasługują na takie
specjalne traktowanie.  Przykładowo pliki PDF mogą być otwierane
normalnie.  Tak samo pliki ZIP czy zwykłe obrazki.

Doszedłem do wniosku, że tak na prawdę jest tylko kilka przypadków,
które chce tak wspierać.  Dwa główne to katalogi i pliki tekstowe
wszelkiej maści.  Dlatego lepiej będzie mieć wyjątek na pliki
specjalnie traktowane zamiast wyjątek na pliki traktowane normalnie.

<2020-01-05 nie 09:53>

Mam obsługę widoku.  Podczas uruchamiania serwera wybrane pliki
(obecnie tylko pliki tekstowe i katalogi) są przetwarzane na pliki
HTML z użyciem prostego szablonu widoku.

Prezentuje się to tragicznie.  Jednak zanim zatroszczę się o walory
estetyczne chcę dodać możliwość otworzenia tych wszystkich plików
normalnie, bez widoku.

Potem zacznie się zabawa bo tak na prawdę serce całego projektu to
właśnie ta główna funkcjonalność, która powoduje, że linki do
wybranych typów zwykłych plików są obsługiwane przez widok.

<2020-01-05 nie 12:12>

Mamy to!  Zrobiłem tak, że jeśli poprzedzi się ścieżkę do pliku
katalogiem "/src" to serwer prześle oryginalny plik.  Oryginalne pliki
są zapisywane w osobnym katalogu podczas uruchamiania serwera.  Nie
jestem super zadowolony z pomysłu i wykonania, ale na chwilę obecną
nie mam nic lepszego w głowie.  To jest dość proste i działa.

Teraz mogę przejść do pracy nad samym widokiem.

<2020-01-05 nie 17:50>

Zastanawiam się co jest teraz najważniejsze.  Myślę, że drobne
ostylowanie tego co już mam.  Potem na pewno będzie trzeba stworzyć
osobne szablony dla poszczególnych typów plików.  To jeszcze wymaga
przemyślenia.  Na razie style.

<2020-01-05 nie 18:18>

Poważnie rozważam wrzucenie już tego na serwer zamiast obecnej
strony.  Jest ubogie ale do wyświetlania treści już się nadaje.

Zastanawiam się jeszcze nad znacznym usprawnieniem procesu tworzenia
statycznych plików.  Teraz wszystko jest kopiowane i zapisywane
w dwóch katalogach serwera.  Proces jest bardzo niewydajne zwłaszcza
podczas kopiowania dużej ilości obrazków i innych plików których i tak
nie przerabiam w żadne sposób.  Tak nie może być.  Przecież ja tam
będę miał GB filmów, starych tutoriali itp.  Trzeba to zmodyfikować.

Najlepiej byłoby ustawiać katalog plików publicznych bezpośrednio na
wskazaną ścieżkę plików źródłowych.  W osobnym katalogu tworzyć pliki
oparte na szablonie widoku.  Tak zrobię, ale w następnej złotej
godzinie.  Teraz trochę uprzątam i zrobię commita.

<2020-01-05 nie 18:32>

Gdyby jakiś developer zobaczył kod tego serwera to zapewne dostałby
ataku serca.  Wszystko w jednym pliku, bez frameworków, widok to jedna
zmienna i w ogóle nie ma niczego do obsługi szablonów.  Style wpisane
bezpośrednio w szablon, bez spacji, bez znaków nowej linii no.  No
i najważniejsze - brak średników haha ;d  Żadnego systemu do budowania
projektu, brak testów.  Jeszcze gdyby się dowiedzieli, że pisałem to w
Emacsie bez żadnego podpowiadania kodu, często bez kolorowania
składni, na pracując na jednym monitorze.  Niemożliwe, że taki web dev
pracuje na co dzień w poważnej firmie ;d

<2020-01-08 śro 20:06>

Zupełnie zapomniałem włączyć minutnik odliczający złotą godzinę.  Już
dobre kilkanaście minut modyfikuję kod serwera zgodnie z tym co
ostatnim razem napisałem.  No trudno.  Włączam odliczanie od teraz.

<2020-01-08 śro 20:24>

Działa.  Uruchamia się też znacznie szybciej bo w ogóle pomijany jest
proces kopiowania.  Teraz mogę trochę posprzątać kod o ile w ogóle
będzie taka potrzeba.

<2020-01-08 śro 20:50>

Napisałem własną funkcję do pobierania rozszerzenia pliku na podstawie
jego nazwy lub ścieżki.  Poprzednie rozwiązanie z wykorzystaniem
metody biblioteki "path" było wadliwe.  Arek to zauważył jak
czytaliśmy kod źródłowy tej metody.  Problem polegał na tym, że
możliwe jest posiadanie katalogu z kropką w nazwie.  I teraz
przekazując do metody ścieżkę "path/to.dir/" dostaniemy w odpowiedzi
".dir" co moim zdaniem jest błędne.

Mniejsza o to.  Teraz powinno być wszystko dobrze.  Zapewne ten kod
nie zadziałałby na Windowsie gdzie ścieżki do plików używają
backslesha.  Właściwie mógłbym to też obsłużyć ... a tam szkoda
czasu.  Obsłużę jeśli będzie taka potrzeba.  Nie ma co komplikować.

<2020-01-09 czw 14:27>

Zastanawiam się co teraz robić i chyba nie pozostało mi nic innego jak
wrzucić to na serwer.  Przygotować prawdziwą treść i powoli przenosić
się na nową stronę.

Na początek muszę odnowić moje konto z serwerem SSH.  Potem powinno
być z górki, no ale to się okaże.

<2020-01-09 czw 14:32>

Oczywiście nie mogę połączyć się z serwerem bo nie płaciłem za niego
od kilku miesięcy.  Trzeba to uregulować.

<2020-01-09 czw 14:54>

Zapłacone, ale wygląda na to, że stary serwer został całkowicie
usunięty.  Ostatnim razem jak miałem taką sytuację to był tylko
wyłączony.  No cóż.  Tym razem przesadziłem z długością okresu bez
wnoszenia opłat.  Trzeba zrobić wszystko na nowo.

<2020-01-09 czw 15:50>

Serwer postawiony.  Dodałem nowego użytkownika z dostępem do "sudo".
Nigdy nie pamiętam jak się to robi.  Dlatego poniżej zostawiam krótką
notatkę z pierwszych wykonanych operacji.

        $ ssh root@<ip>
        $ adduser turtle
        $ apt update
        $ apt install sudo
        $ usermod -aG sudo turtle
        $ groups turtle # -> turtle : turtle sudo
        $ ssh turtle@<ip>

Od tej pory wszystko robię z użyciem użytkownika "turtle".
Zainstalowałem program git i nvm.  Skonfigurowałem klucz SSH do
GitLab'a i sklonowałem projekt na serwer.  Odpaliłem i dział!

Przeglądarka bardzo krzyczy jak podaję adres poprzez ip, ale da się to
zignorować i wejść na stronę.

Następnym krokiem będzie dodanie treści.  Zacznę od plików z mojego
serwera FTP.  Na pewno będę chciał mieć też kilka rzeczy, widnieją na
mojej stronie http://gumen.pl takie jak CV i linki do kilku stron.

Wypełnienie serwera plikami pozwoli mi określić priorytety następnych
rzeczy jakie chce dodać do tego projektu.

<2020-01-11 sob 12:31>

Próbowałem już wrzucać jakieś pliki na serwer.  SSH nie jest
najwygodniejszym sposobem na przenoszenie dużych plików z serwera
FTP.  Dlatego chcę skonfigurować serwer FTP na nowym serwerze strony.
Nie mam pojęcia jak to się robi.  Chyba wiem gdzie szukać.

https://www.youtube.com/watch?v=VE-2GHwUJzE

Miałem też problem z uruchomieniem tego projektu na nowym serwerze
strony.  Odpalenie zwykłego NodeJS skutkowało zamknięciem aplikacji po
jakimś czasie.  Nie wiem czym to było spowodowane.  Użyłem pakietu
"pm2" do uruchomienia projektu.  Nie było to jednak takie proste.  Oto
komenda jakiej użyłem

$ pm2 start npm -- run start -- /home/turtle/public_server_files/

<2020-01-11 sob 12:46>

Zainstalowałem program "proftpd".  Nic nie musiałem robić i FTP już
działa.  Przystępuję do przenoszenia plików.  Przy okazji chcę
przenieść różne pliki z starego serwera FTP na ThinkPada.  Do wielu
z tych plików nie mam nigdzie kopi zapasowej.

<2020-01-11 sob 13:04>

Zorientowałem się też, że nie mam zdefiniowanego "content-type" dla
plików mp4.  Trzeba to dodać.

<2020-01-11 sob 13:06>

To zajmie całe wieki.  Kopiowanie filmów i oczyszczanie starego
serwera.  Tam jest bardzo dużo rzecze.  Muszę zrobić z tym porządek bo
chciałbym powoli z niego rezygnować.

Niestety postawiłem na nim kilka stron dla znajomych.  Przeniesienie
ich to grubsza sprawa dlatego teraz zrobię co się da tylko ze swoimi
rzeczami.  Zostawię same strony, które są używane.

<2020-01-14 wto 13:56>

Wracam do przenoszenia plików.  Ostatnim razem dodałem wsparcie dla
różnych formatów plików wideo, ale nawet tego nie przetestowałem.
Sprawdzę to wszystko lokalnie.

Zdecydowałem też, że na chwilę obecną wykluczę z listy plików
tekstowych pliki HTML.

<2020-01-14 wto 14:47>

Wrzucam dużo filmów na serwer.  Stare tutoriale, nagrania z 3D, rzeczy
z Gothica itp.  Jest tego dość dużo.  Na serwerze mam dostępne 20 GB.
Na chwilę obecną same filmy zajmują 4 GB.  Na razie nie zapowiada się
aby jakieś przybyły, no ale zobaczymy.

Generalnie muszę bardziej uważać z ilością zajmowanego miejsca.  Nie
boję się o pliki tekstowe i obrazki, ale na pewno będę miał katalog
"files" z różnymi różnościami, które mogę sporo ważyć.

<2020-01-15 śro 23:53>

Myślę, że najwyższa pora ustawić domenę na adres nowego serwera.
Zacznę od tego i potem zobaczę co chciałbym jeszcze dodać na stronę.

<2020-01-16 czw 00:00>

Robiłem to tylko raz jakiś rok temu.  Nie specjalnie się na tym znam,
ale to co zrobiłem to dokonałem modyfikacji rekordu A dla domeny
gumen.pl ustawiając w nim IP nowego serwera.  Zmiana nigdy nie jest
widoczna od razu dlatego muszę trochę poczekać aby sprawdzić się czy
wszystko działa.

<2020-01-16 czw 00:48>

Ok, wszystko gotowe.  Domena jeszcze nie działa, ale wygląda na to, że
większym dla mnie problemem będzie uruchomienie serwera na porcie 80.
Jest to standardowy port dla stron bez zabezpieczeń.  Użytkownik nie
będący rootem nie ma uprawnień żeby uruchomić program na tym porcie.

                                * * *

Poczytałem trochę na ten temat.  Sposobów jest wiele.
Najpopularniejsze rozwiązanie to przekierowanie portów.  Można też
ustawić aplikacje uruchamiane w NodeJS jako bezpieczne.  Tak też
zrobiłem.  Używając tych komend uruchomiłem serwer na porcie 80:

$ sudo apt-get install libcap2-bin
$ sudo setcap cap_net_bind_service=+ep `readlink -f \`which node\``
$ PORT=80 pm2 start npm -- run start -- /home/turtle/public_server_files/

Źródło:
https://www.digitalocean.com/community/tutorials/how-to-use-pm2-to-setup-a-node-js-production-environment-on-an-ubuntu-vps#give-safe-user-permission-to-use-port-80

<2020-01-16 czw 10:09>

Domena działa!  Super bo teraz mogę łatwiej na nią wchodzić, logować
się przez SSH i FTP.  Wrzucam różne pliki i modyfikuję treść.
Wiedziałem, że jak tylko zastąpię starą stronę nową to zacznę szybko
zauważać co na niej potrzebuję, albo co chciałbym mieć.  Pierwsze
rzecz, która stała się oczywista niemalże natychmiast, to konieczność
posiadania mechanizmu automatycznie restartującego serwer kiedy
pojawią się zmiany w plikach.  Od tego zacznę jak następnym razem będę
pracował nad kodem strony.  Następnie będę chciał wyświetlać więcej
informacji o plikach w widoku katalogów.  Ich formaty, rozmiary i daty
modyfikacji.  Wtedy przyjdzie czas na stylowanie.  Będę musiał
popracować nad tym jak wyświetlane są pliki tekstowe.  Chcę żeby ich
przeglądanie na telefonie było bardzo wygodne.  Przewiduję opcje
zwiększania i zmniejszania stopnia pisma jak i możliwość zawijania
wierszy.  Dopiero teraz zacznie się prawdziwa zabawa.
