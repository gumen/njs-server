/*
  "The Cave"

  Engine: There is something not right here.  I feel cold.

  Coder: That place, is strong in dark side of the code.  Domain of
  impurity it is.  And You must execute.

  Engine: What's in there?

  Coder: Only side effects.
*/

import http from 'http'
import url from 'url'
import path from 'path'
import { promises as fs } from 'fs'
import mimeTypes from './mime_types.json'

const cwd = process.cwd()
const port = process.env.PORT || 8000
const viewPath = path.join(cwd, 'source')

// List of files that should be displayed using view
const viewFiles = ['', 'css', 'csv', 'htm', 'js', 'json', 'jsonld', 'md', 'mjs', 'org', 'php', 'sh', 'txt', 'xhtml', 'xml', 'text']

const viewTemplate = (content) => `
  <!DOCTYPE html>
  <html lang="en" id="home">
    <head>
      <meta charset="utf-8" />
      <meta name="theme-color" content="#000000" />
      <meta name="msapplication-navbutton-color" content="#000000" />
      <meta name="apple-mobile-web-app-status-bar-style" content="#000000" />
      <meta name="apple-mobile-web-app-capable" content="yes" />
      <meta name="mobile-web-app-capable" content="yes" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <meta name="author" content="gumen" />
      <meta name="description" content="Programmer's mind, artist's soul and warrior's heart" />
      <title>gumen</title>
      <style>
        html{line-height:1.7;background:#f6f6f6;color:#333;font-size:calc(14px + .2vw)}
        body{margin:4em 4vw;max-width:100vw}
        a{color:gray}
        a:visited{color:currentColor}
        a:focus{outline:0;box-shadow:none;background:#000;color:#fff!important;text-decoration:none}
        h1,h2,h3,h4,h5,h6{font-family:Helvetica,Verdana,Geneva,sans-serif;line-height:1.2}
        h1{font-size:1em}
        main{
          width:50em;
          max-width:100%;
          border:1px solid black;
          padding:.5em 1em;
          box-sizing:border-box;
          background:white;
          overflow:auto}
      </style>
    </head>
    <body>
      <header>
        <h1><a href="/" title="🐢🐢 Work at a turtle's pace. Overtake rabbits!">gumen</a></h1>
      </header>
      <main>
        ${content}
      </main>
      <footer>
      </footer>
    </body>
  </html>
`

let publicPath

/**
 * Return file extension based on file path or file name.
 * @param {String} f - Path to file or file name with extansion.
 * @return {String} File extansion without leading dot.
 */
function getExtname (f) {
  const fileName = f.split('/').pop()
  const dotsCount = (fileName.match(/\./g) || []).length

  // Return empty string if file don't have extension.  It might be
  // possible if there is not dots in file name or there is only one
  // at the beginning (hidden file with no extension).
  if (dotsCount < 1 || dotsCount === 1 && fileName[0] === '.') return ''

  return fileName.split('.').pop().toLowerCase()
}

const server = http.createServer({}, async (req, res) => {
  let pathname = decodeURIComponent(url.parse(req.url).pathname)
  let extname = getExtname(pathname)
  let pathRoot = publicPath

  if (pathname.startsWith('/src/')) {
    pathname = pathname.replace(/\/src/, '')
  } else if (viewFiles.includes(extname)) {
    extname = 'html'
    pathRoot = viewPath
  }

  // Make path safe by resolving it to '/' and replacing all '~' characters
  let filePath = path.join(pathRoot, path.resolve('/', pathname.replace(/~/g, '')))

  try {
    const fileStats = await fs.stat(filePath, { bigint: false })
    if (fileStats.isDirectory()) filePath = path.join(filePath, 'index.html')
    const file = await fs.readFile(filePath)

    res.setHeader('Content-Type', mimeTypes[extname] || mimeTypes['txt'])
    res.statusCode = 200
    res.end(file)
  } catch (err) {
    console.error('>> error:', err)
    res.statusCode = 404
    res.end('404')
  }
})

process.on('SIGINT', () => { server.close() })
process.on('exit', c => { console.log(`\nExit code: ${c}`) })

/**
 * Return true if path exists and file / dir is accessible.
 * @param {String} p - Path to file or directory.
 * @return {Promise} Resolve with true if accessible, false if not.
 */
async function access (p) {
  return await fs.access(p)
    .then(() => true)
    .catch(() => false)
}

/**
 * Recursive remove directory under given path.
 * @param {String} dirPath
 */
async function rmdir (dirPath) {
  if (!await access(dirPath)) return
  const files = await fs.readdir(dirPath)

  for (const fileName of files) {
    const filePath = path.join(dirPath, fileName)
    const fileStats = await fs.stat(filePath, { bigint: false })

    if (fileStats.isDirectory()) await rmdir(filePath)
    else await fs.unlink(filePath)
  }

  await fs.rmdir(dirPath)
}

/**
 * Prepare static files + some MAGIC!
 * @param {String} src
 * @param {String} dist
 */
async function processSourceDir (src, dist) {
  if (!src) {
    console.error(new Error('server/processSourceDir missing source path, src:', src))
    return
  }

  if (!dist) {
    console.error(new Error('server/processSourceDir missing destination path, dist:', dist))
    return
  }

  if (!await access(src)) {
    console.error(new Error('server/processSourceDir cannot access source path', src))
    return
  }

  await fs.mkdir(path.join(viewPath, dist))
  const files = await fs.readdir(src)

  for (const fileName of files) {
    const filePath = path.join(src, fileName)
    const fileStats = await fs.stat(filePath, { bigint: false })
    const fileDist = path.join(dist, fileName)

    if (fileStats.isDirectory()) {
      await processSourceDir(filePath, fileDist)
    } else if (viewFiles.includes(getExtname(fileName))) {
      await fs.writeFile(path.join(viewPath, fileDist), viewTemplate(`<pre>${await fs.readFile(filePath)}</pre>`))
    }
  }

  // Create index.html file for given directory if don't exist
  if (!files.includes('index.html')) {
    await fs.writeFile(path.join(viewPath, dist, 'index.html'), viewTemplate(`
      <ul>
        ${files.map(f => `<li><a href="${path.join(dist, f)}">${f}</a></li>`).join('')}
      </ul>
    `))
  }
}

(async function () {
  if (process.argv.length < 3) { // First two arguments are always present
    console.error(new Error('Missing argument - path to server public files'))
    return
  }

  publicPath = process.argv[process.argv.length - 1]

  if (!await access(publicPath)) {
    console.error(new Error(`Provided public path "${publicPath}" is incorrect`))
    return
  }

  await rmdir(viewPath)
  await processSourceDir(publicPath, '/')

  server.listen(port, () => {
    console.log(`Fallow white rabbit at http://localhost:${port}`)
  })
}())
